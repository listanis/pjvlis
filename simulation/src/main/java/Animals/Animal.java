package Animals;

import Map.Cell;
import Map.Time;

import java.util.Objects;
import java.util.Random;

import static java.lang.Math.abs;

/**
 * This class represents an animal in the simulation. Animals can move, hunt, eat, and reproduce.
 */
public class Animal implements Runnable {

    /**
     * Gets the animal's name.
     *
     * @return The animal's name.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the animal's movement speed.
     *
     * @return The animal's movement speed.
     */
    public int getMoveSpeed() {
        return moveSpeed;
    }

    /**
     * Sets the animal's movement speed.
     *
     * @param moveSpeed The new movement speed.
     */
    public void setMoveSpeed(int moveSpeed) {
        this.moveSpeed = moveSpeed;
    }

    /**
     * Sets the number of days the animal has gone without food.
     *
     * @param daysWithoutFood The number of days without food.
     */
    public void setDaysWithoutFood(int daysWithoutFood) {
        this.daysWithoutFood = daysWithoutFood;
    }

    /**
     * Gets the animal's attack power.
     *
     * @return The animal's attack power.
     */
    public int getPower() {
        return power;
    }

    /**
     * Gets the animal's X coordinate on the map.
     *
     * @return The animal's X coordinate.
     */
    public int getX() {
        return x;
    }

    /**
     * Sets the animal's X coordinate on the map.
     *
     * @param x The new X coordinate.
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * Gets the animal's Y coordinate on the map.
     *
     * @return The animal's Y coordinate.
     */
    public int getY() {
        return y;
    }

    /**
     * Sets the animal's Y coordinate on the map.
     *
     * @param y The new Y coordinate.
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * Gets the animal's current health.
     *
     * @return The animal's health.
     */
    public int getHealth() {
        return health;
    }

    protected String name;
    protected int moveSpeed;
    protected int daysWithoutFood = 0;
    protected int power; // If animal attacks someone with lower power, the attacked animal dies. Otherwise, the attacking animal dies.

    protected int activeMoveSpeed;
    public int x;
    public int y;
    public boolean readyToCreate;
    protected int health; // If health is below 20%, movement speed decreases significantly.
    final int defaultHealth;

    public static Cell[][] map;

    public int visionRadius = 10;

    public static Time time;
    public volatile boolean running = false;

    public boolean isAlive = true;
    public int dayOfLastEating;

    /**
     * Creates a new Animal object.
     *
     * @param moveSpeed The animal's movement speed.
     * @param name The animal's name.
     * @param power The animal's attack power.
     * @param startX The animal's starting X coordinate.
     * @param startY The animal's starting Y coordinate.
     * @param health The animal's starting health.
     * @param defaultHealth The animal's default health.
     * @param map The map where the animal will live.
     * @param time The simulation time object.
     */
    public Animal(int moveSpeed, String name, int power, int startX, int startY, int health, int defaultHealth, Cell[][] map, Time time) {
        this.x = startX;
        this.y = startY;
        this.moveSpeed = moveSpeed;
        this.name = name;
        this.power = power;
        this.health = health;
        this.defaultHealth = defaultHealth;
        this.map = map;
        this.time = time;
    }

    public Animal(int defaultHealth) {
        this.defaultHealth = defaultHealth;
    }
    /**
     * This method is used to move the animal in a specific direction.
     * @param direction The direction in which the animal should move.
     */
    public void move(Direction direction) {
        if (this.health <= 0) {
            return;
        }else{
            switch (direction) {
                case UP:
                    if (y + moveSpeed < map.length && !map[x][y + moveSpeed].containsAnimal()) {
                        map[x][y].containsAnimal = false;
                        map[x][y].animalOnCell = null;
                        map[x][y + moveSpeed].animalOnCell = this;
                        map[x][y + moveSpeed].containsAnimal = true;

                        this.y += moveSpeed;
                    }
                    break;
                case DOWN:
                    if (y - moveSpeed >= 0 && !map[x][y - moveSpeed].containsAnimal()) {
                        map[x][y].containsAnimal = false;
                        map[x][y].animalOnCell = null;
                        map[x][y - moveSpeed].animalOnCell = this;
                        map[x][y - moveSpeed].containsAnimal = true;

                        this.y -= moveSpeed;
                    }
                    break;
                case RIGHT:
                    if (x + moveSpeed < map[0].length && !map[x + moveSpeed][y].containsAnimal()) {
                        map[x][y].containsAnimal = false;
                        map[x][y].animalOnCell = null;
                        map[x + moveSpeed][y].animalOnCell = this;
                        map[x + moveSpeed][y].containsAnimal = true;

                        this.x += moveSpeed;
                    }
                    break;
                case LEFT:
                    if (x - moveSpeed >= 0 && !map[x - moveSpeed][y].containsAnimal()) {
                        map[x][y].containsAnimal = false;
                        map[x][y].animalOnCell = null;
                        map[x - moveSpeed][y].animalOnCell = this;
                        map[x - moveSpeed][y].containsAnimal = true;
                        this.x -= moveSpeed;
                    }
                    break;
                default:
                    break;

            }
        }
    }
    /**
     * This method is used when the animal dies. It sets the health to 0 and removes the animal from the map.
     */
    public void dieForever() {
        health = 0;
        map[x][y].animalOnCell = null;
        map[x][y].containsAnimal = false;
        moveSpeed = 0;
        System.out.println(name + " has died");
        isAlive = false;
    }

    public void attack(Animal animal) {
        if(animal.health - power <= 0){
            animal.dieForever();
            map[x][y].marked = true;
        }
        else{
            animal.health -= power;
        }
        System.out.println(name + " attacked " + animal.name);
    }

    public void sleep() {
        activeMoveSpeed = moveSpeed;
        moveSpeed = 0;
    }

    public Animal findAnimal() {
        Animal nearestPrey = null;
        double minDistance = Double.MAX_VALUE;
        for (int i = Math.max(0, x - visionRadius); i <= Math.min(map[0].length - 1, x + visionRadius); i++) {
            for (int j = Math.max(0, y - visionRadius); j <= Math.min(map.length - 1, y + visionRadius); j++) {
                if (map[i][j].containsAnimal() && !Objects.equals(map[i][j].animalOnCell.getName(), name)) {
                    nearestPrey = map[i][j].animalOnCell;
                    return nearestPrey;
                }
            }
        }
        return nearestPrey;
    }

    public void hunt() {
        Animal animalToHunt = findAnimal();
        if (animalToHunt != null) {
            int animalX = animalToHunt.x;
            int animalY = animalToHunt.y;
            if (animalToHunt.health <= 0) {
                System.out.println(animalToHunt.name +" was eaten");
                eat(animalToHunt);
                readyToCreate=true;
                daysWithoutFood = 0;

            } else if (abs(animalY - y) <= 1 && abs(animalX - x) <= 1) {
                attack(animalToHunt);
            } else {
                if (animalX > x && x + moveSpeed < map.length && !map[x + moveSpeed][y].containsAnimal()) {
                    move(Direction.RIGHT);
                } else if (animalX < x && x - moveSpeed >= 0 && !map[x - moveSpeed][y].containsAnimal()) {
                    move(Direction.LEFT);
                } else if (animalY > y && y + moveSpeed < map[0].length && !map[x][y + moveSpeed].containsAnimal()) {
                    move(Direction.UP);
                } else if (animalY < y && y - moveSpeed >= 0 && !map[x][y - moveSpeed].containsAnimal()) {
                    move(Direction.DOWN);
                }
            }
        }
        else{
            walk();
        }
    }

    public void wakeUp() {
        moveSpeed = activeMoveSpeed;
    }

    public void eat(Animal animal) {
        this.health = defaultHealth;
        animal.dieForever();
        dayOfLastEating = time.days;
    }

    public void walk() {
        if(health > 0){
            int borderX = map.length;
            int borderY = map[0].length;
            Random rand = new Random();
            Direction randomDirection = Direction.values()[rand.nextInt(Direction.values().length)];
            move(randomDirection);
        }
    }

    public void createNewAnimal(Animal animal){
        if(!map[x][y].marked){
            map[x][y].marked = true;
        } else{
            walk();
        }
    }

    public void stopRunning() {
        running = true;
    }

    public void attackIfClose() {
        for (int i = Math.max(0, x - 1); i <= Math.min(map.length - 1, x + 1); i++) {
            for (int j = Math.max(0, y - 1); j <= Math.min(map[0].length - 1, y + 1); j++) {
                if (map[i][j].containsAnimal() && !(i == x && j == y)) {
                    Animal otherAnimal = map[i][j].getAnimalOnCell();
                    if (!otherAnimal.getName().equals(getName())) {
                        attack(otherAnimal);
                        return;
                    }
                }
            }
        }
    }


    @Override
    public void run() {

    }

}