package Animals;


import Map.Cell;

import java.util.Random;

/**
 * Represents a Lion in the simulation, which is a type of Animal.
 */
public class Lion extends Animal {

    /**
     * Name of the lion.
     */
    String name = "Lion";

    /**
     * Random number generator for determining initial move speed and power.
     */
    Random rand = new Random();

    /**
     * Creates a new Lion at the specified position.
     *
     * @param positionX The X-coordinate of the initial position.
     * @param positionY The Y-coordinate of the initial position.
     * @param map       Reference to the map cells.
     */
    public Lion(int positionX, int positionY, Cell[][] map) {
        super(100); // Default health for lion
        super.x = positionX;
        super.y = positionY;
        super.moveSpeed = rand.nextInt(1) + 1; // Random initial move speed
        super.name = name;
        super.health = 100; // Default health for lion
        super.power = rand.nextInt(50) + 100; // Random initial power
        super.name = "Lion";
        super.map = map;
    }

    @Override
    public void run() {
        if(dayOfLastEating < time.days)
            daysWithoutFood+=1;
        if(isAlive){
            attackIfClose(); // Check for nearby animals to attack
        // Lion's behavior when days without food exceed a certain threshold
        if (daysWithoutFood >= 1) {
            hunt(); // Lions walk when hungry
        } else if (daysWithoutFood != 2) {
            walk();
        } else {
            System.out.println(name + " died from hunger");
            this.dieForever(); // Lion dies if it hasn't eaten for 2 days
        }
        }
    }

}
