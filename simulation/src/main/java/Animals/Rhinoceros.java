package Animals;

import Map.Cell;

import java.util.Random;

/**
 * Represents a Rhinoceros in the simulation, which is a type of Animal.
 */
public class Rhinoceros extends Animal {

    /**
     * Name of the rhinoceros.
     */
    String name = "Rhinoceros";

    /**
     * Random number generator for determining initial move speed and power.
     */
    Random rand = new Random();

    /**
     * Flag indicating whether the rhinoceros is angry.
     */
    boolean isAngry = false;

    /**
     * Creates a new Rhinoceros at the specified position.
     *
     * @param positionX The X-coordinate of the initial position.
     * @param positionY The Y-coordinate of the initial position.
     * @param map       Reference to the map cells.
     */
    public Rhinoceros(int positionX, int positionY, Cell[][] map) {
        super(120); // Default health for rhinoceros
        this.x = positionX;
        this.y = positionY;
        super.moveSpeed = rand.nextInt(1) + 1; // Random initial move speed
        super.power = rand.nextInt(50) + 100; // Random initial power
        super.health = 120; // Default health for rhinoceros
        super.name = "Rhinoceros";
        super.map = map;
    }

    /**
     * Makes the rhinoceros angry, increasing its power and move speed.
     */
    public void Rampage() {
        power += 30; // Increase power when angry
        moveSpeed += 4; // Increase move speed when angry
        isAngry = true; // Set the angry flag
    }

    @Override
    public void run() {
        if(isAlive){
        attackIfClose(); // Check for nearby animals to attack
        walk();
        }
    }
}
