package Application;

import Controllers.AnimalController;
import Controllers.MapController;
import Controllers.SimulationController;
import Map.MapRenderer;
import Map.Time;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class SimulationApplication extends Application {

    private boolean sizeSelected = false;
    private int mapWidth = 0;
    private int mapHeight = 0;
    private Timer timer;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        showStartSimulationDialog(primaryStage);
    }

    private void showStartSimulationDialog(Stage primaryStage) {
        Stage dialogStage = new Stage();
        dialogStage.initModality(Modality.WINDOW_MODAL);
        dialogStage.initOwner(primaryStage);

        Button startButton = new Button("Start Simulation");
        startButton.setDisable(true);

        Button smallButton = new Button("Small");
        Button mediumButton = new Button("Medium");
        Button largeButton = new Button("Large");

        smallButton.setOnAction(event -> {
            sizeSelected = true;
            mapWidth = 60;
            mapHeight = 60;
            startButton.setDisable(false);
        });

        mediumButton.setOnAction(event -> {
            sizeSelected = true;
            mapWidth = 80;
            mapHeight = 80;
            startButton.setDisable(false);
        });

        largeButton.setOnAction(event -> {
            sizeSelected = true;
            mapWidth = 90;
            mapHeight = 90;
            startButton.setDisable(false);
        });

        startButton.setOnAction(event -> {
            dialogStage.close();
            startSimulation(primaryStage, mapWidth, mapHeight);
        });

        HBox buttonsBox = new HBox(10, smallButton, mediumButton, largeButton);
        buttonsBox.setAlignment(Pos.CENTER);
        VBox vbox = new VBox(20, buttonsBox, startButton);
        vbox.setAlignment(Pos.CENTER);
        vbox.setStyle("-fx-padding: 10px;");

        Scene scene = new Scene(vbox, 300, 200);
        dialogStage.setScene(scene);
        dialogStage.show();
    }

    private void startSimulation(Stage primaryStage, int mapWidth, int mapHeight) {
        if (timer != null) {
            timer.cancel();
        }

        int squareSize;
        int lionCount, elephantCount, cheetahCount, rhinoCount, zebraCount;

        if (mapWidth == 60 && mapHeight == 60) {
            squareSize = 23;
            lionCount = 5;
            elephantCount = 5;
            cheetahCount = 5;
            rhinoCount = 5;
            zebraCount = 40;
        } else if (mapWidth == 80 && mapHeight == 80) {
            squareSize = 17;
            lionCount = 8;
            elephantCount = 8;
            cheetahCount = 8;
            rhinoCount = 8;
            zebraCount = 80;
        } else {
            squareSize = 16;
            lionCount = 10;
            elephantCount = 10;
            cheetahCount = 10;
            rhinoCount = 10;
            zebraCount = 100;
        }
        Time timeMode = new Time("real_time");
        MapController mapController = new MapController(mapWidth, mapHeight, lionCount, cheetahCount, rhinoCount, zebraCount, elephantCount, timeMode);

        MapRenderer mapRenderer = new MapRenderer(mapController, timeMode, squareSize);
        AnimalController animalController = new AnimalController(timeMode, mapController);

        SimulationController simulationController = new SimulationController();
        simulationController.setTime(timeMode);
        simulationController.setMapController(mapController);

        try {
            mapRenderer.initializeMapView(primaryStage);

            timer = new Timer();

            TimerTask task = new TimerTask() {
                public void run() {
                    Platform.runLater(() -> {
                        try {
                            timeMode.tick();
                            mapRenderer.updateMapView();
                            animalController.updateCurrentChunk();
                            simulationController.updateTime();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    });
                }
            };
            timer.scheduleAtFixedRate(task, 0, 1500);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

