package Controllers;

import Animals.Animal;
import Map.Chunk;
import Map.Time;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class is responsible for controlling the map of the simulation.
 * It interacts with the MapCreator to create the map and distribute animals, and manages chunks of the map.
 */
public class MapController {
    private final Time time;
    private int width;
    private int height;
    private int lions;
    private int cheetahs;
    private int zebras;
    private int rhinoceros;
    private int elephants;
    public MapCreator mapCreator;

    public Chunk[] chunks = new Chunk[4];
    public int currentChunkIndex = 0;
    public List<Animal> currentChunkAnimals;
    private boolean[] animalsAddedToChunk = new boolean[4];

    /**
     * Constructor for the MapController class. Initializes a new MapController with the specified parameters.
     *
     * @param width The width of the map.
     * @param height The height of the map.
     * @param lions The number of lions in the simulation.
     * @param cheetahs The number of cheetahs in the simulation.
     * @param rhinoceros The number of rhinoceros in the simulation.
     * @param zebras The number of zebras in the simulation.
     * @param elephants The number of elephants in the simulation.
     */
    public MapController(int width, int height, int lions, int cheetahs, int rhinoceros, int zebras, int elephants, Time time) {
        this.width = width;
        this.height = height;
        this.lions = lions;
        this.cheetahs = cheetahs;
        this.zebras = zebras;
        this.elephants = elephants;
        this.rhinoceros = rhinoceros;
        this.mapCreator = new MapCreator(width, height, lions, rhinoceros, cheetahs, zebras, elephants, time);
        this.currentChunkAnimals = new ArrayList<>();
        Arrays.fill(animalsAddedToChunk, false);
        this.time = time;
    }
    /**
     * This method creates chunks, initialize chunks borders.
     */
    private void createChunks() {
        int halfWidth = width / 2;
        int halfHeight = height / 2;

        chunks[0] = new Chunk(0, 0, halfWidth - 1, halfHeight - 1);
        chunks[1] = new Chunk(halfWidth, 0, width - 1, halfHeight - 1);
        chunks[2] = new Chunk(0, halfHeight, halfWidth - 1, height - 1);
        chunks[3] = new Chunk(halfWidth, halfHeight, width - 1, height - 1);
    }
    /**
     * This creates map, fill it and create chunks.
     */
    public void createMap() {
        mapCreator.createMap();
        mapCreator.fillMap(mapCreator.listOfAmountOfAnimals);
        createChunks();
        distributeAnimals();
        loadChunk(currentChunkIndex);
    }
    /**
     * This method checks adds all the animals of current chunk.
     */
    private void distributeAnimals() {
        List<Animal> animals = mapCreator.animalList;
        for (Animal animal : animals) {
            int chunkIndex = getChunkIndex(animal.getX(), animal.getY());
            if (chunkIndex != -1) {
                chunks[chunkIndex].addAnimal(animal);
            }
        }
    }
    /**
     * This method checks if an animal is inside the current chunk of the map.
     *
     * @param x,y coordinates.
     * @return int index of chunks of given coordinates.
     */
    private int getChunkIndex(int x, int y) {
        for (int i = 0; i < chunks.length; i++) {
            if (chunks[i].contains(x, y)) {
                return i;
            }
        }
        return -1;
    }
    /**
     * This method is used to load the current chunk of the map.
     */
    public void loadChunk(int chunkIndex) {
        if (chunkIndex >= 0 && chunkIndex < chunks.length) {
            currentChunkIndex = chunkIndex;
            currentChunkAnimals.clear();
            currentChunkAnimals.addAll(chunks[currentChunkIndex].getAnimals());
            if (currentChunkIndex != -1) {
                Arrays.fill(animalsAddedToChunk, false);
                animalsAddedToChunk[currentChunkIndex] = true;
            }
        }
    }
    /**
     * This method is used to update the current chunk of the map.
     * It checks each animal in the current chunk and updates its running state based on whether it is inside the chunk.
     */
    public void updateCurrentChunk() {
        Chunk currentChunk = chunks[currentChunkIndex];
        for (Animal animal : currentChunk.getAnimals()) {
            if (!isInsideChunk(animal)) {
                if(animal.running) {
                    animal.stopRunning();
                }
            } else if(!animal.running) {
                animal.run();
            }
        }
    }
    /**
     * This method checks if an animal is inside the current chunk of the map.
     *
     * @param animal The animal to check.
     * @return true if the animal is inside the current chunk, false otherwise.
     */
    boolean isInsideChunk(Animal animal) {
        Chunk currentChunk = chunks[currentChunkIndex];
        int animalX = animal.getX();
        int animalY = animal.getY();
        return animalX >= currentChunk.getStartX() && animalX <= currentChunk.getEndX() &&
                animalY >= currentChunk.getStartY() && animalY <= currentChunk.getEndY();
    }
}
