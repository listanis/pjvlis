package Map;

import Animals.Animal;

/**
 * Represents a cell in the map grid.
 */
public class Cell extends Field {
    public boolean containsAnimal = false;
    public Terrain terrain = Terrain.GRASS;
    public Animal animalOnCell;

    public boolean marked;
    public String markedAnimalType;
    public int daysBeforeBirth;
    private int x;
    private int y;

    public Time time;

    /**
     * Constructs a Cell object with the specified parameters.
     *
     * @param isThereAnimal Specifies if the cell contains an animal.
     * @param terrain       The terrain type of the cell.
     * @param x             The x-coordinate of the cell.
     * @param y             The y-coordinate of the cell.
     */
    public Cell(boolean isThereAnimal, Terrain terrain, int x, int y) {
        this.x = x;
        this.y = y;
        this.containsAnimal = isThereAnimal;
        this.terrain = terrain;
    }

    /**
     * Checks if the cell contains an animal.
     *
     * @return True if the cell contains an animal, otherwise false.
     */
    public boolean containsAnimal() {
        return animalOnCell != null;
    }

    /**
     * Adds an animal to the cell.
     *
     * @param animal The animal to add.
     */
    public void addAnimal(Animal animal) {
        this.animalOnCell = animal;
        this.containsAnimal = true;
        time = animalOnCell.time;
    }

    /**
     * Removes the animal from the cell.
     */
    public void removeAnimal() {
        this.animalOnCell = null;
        this.containsAnimal = false;
    }

    /**
     * Gets the animal present on the cell.
     *
     * @return The animal on the cell.
     */
    public Animal getAnimalOnCell() {
        return animalOnCell;
    }
}
