package Map;


import Animals.Animal;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a chunk of the map.
 */
public class Chunk {
    private int startX;
    private int startY;
    private int endX;
    private int endY;
    public List<Animal> animals;

    /**
     * Constructs a Chunk object with the specified boundaries.
     *
     * @param startX The starting x-coordinate of the chunk.
     * @param startY The starting y-coordinate of the chunk.
     * @param endX   The ending x-coordinate of the chunk.
     * @param endY   The ending y-coordinate of the chunk.
     */
    public Chunk(int startX, int startY, int endX, int endY) {
        this.startX = startX;
        this.startY = startY;
        this.endX = endX;
        this.endY = endY;
        this.animals = new ArrayList<>();
    }

    /**
     * Gets the list of animals in the chunk.
     *
     * @return The list of animals.
     */
    public List<Animal> getAnimals() {
        return animals;
    }

    /**
     * Updates the chunk by removing animals that are not within its boundaries.
     */
    public void update() {
        for (int i = animals.size() - 1; i >= 0; i--) {
            Animal animal = animals.get(i);
            if (!contains(animal.getX(), animal.getY())) {
                animals.remove(i);
            }
        }
    }

    /**
     * Checks if the chunk contains the specified coordinates.
     *
     * @param x The x-coordinate to check.
     * @param y The y-coordinate to check.
     * @return True if the coordinates are within the chunk, otherwise false.
     */
    public boolean contains(int x, int y) {
        return x >= startX && x <= endX && y >= startY && y <= endY;
    }

    /**
     * Adds an animal to the chunk.
     *
     * @param animal The animal to add.
     */
    public void addAnimal(Animal animal) {
        animals.add(animal);
    }

    /**
     * Gets the starting x-coordinate of the chunk.
     *
     * @return The starting x-coordinate.
     */
    public int getStartX() {
        return startX;
    }

    /**
     * Gets the starting y-coordinate of the chunk.
     *
     * @return The starting y-coordinate.
     */
    public int getStartY() {
        return startY;
    }

    /**
     * Gets the ending x-coordinate of the chunk.
     *
     * @return The ending x-coordinate.
     */
    public int getEndX() {
        return endX;
    }

    /**
     * Gets the ending y-coordinate of the chunk.
     *
     * @return The ending y-coordinate.
     */
    public int getEndY() {
        return endY;
    }
}
