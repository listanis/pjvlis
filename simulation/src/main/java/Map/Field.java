package Map;

/**
 * Represents a generic field.
 */
public class Field {
    private int width;
    private int height;

    /**
     * Constructs a Field object with the specified width and height.
     *
     * @param width  The width of the field.
     * @param height The height of the field.
     */
    public Field(int width, int height) {
        this.width = width;
        this.height = height;
    }

    /**
     * Gets the width of the field.
     *
     * @return The width.
     */
    public int getWidth() {
        return width;
    }

    /**
     * Gets the height of the field.
     *
     * @return The height.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Default constructor.
     */
    public Field() {
        // Default constructor
    }
}
