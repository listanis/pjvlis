package test;


import Animals.Animal;
import Animals.Direction;
import Map.Terrain;
import Map.Time;
import Map.Cell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AnimalTest {

    private Cell[][] map;
    private Time time;

    @BeforeEach
    public void setUp() {
        int mapWidth = 10;
        int mapHeight = 10;
        map = new Cell[mapWidth][mapHeight];
        for (int i = 0; i < mapWidth; i++) {
            for (int j = 0; j < mapHeight; j++) {
                map[i][j] = new Cell(false, Terrain.GRASS,i, j); // Assuming Cell constructor takes x and y coordinates
            }
        }
        time = new Time("real_time");
        Animal.time = time;
        Animal.map = map;
    }

    @Test
    public void testMove() {
        Animal animal = new Animal(1, "TestAnimal", 10, 5, 5, 100, 100, map, time);
        map[5][5].addAnimal(animal);

        animal.move(Direction.UP);
        assertEquals(6, animal.getY());
        assertEquals(5, animal.getX());

        animal.move(Direction.RIGHT);
        assertEquals(6, animal.getY());
        assertEquals(6, animal.getX());

        animal.move(Direction.DOWN);
        assertEquals(5, animal.getY());
        assertEquals(6, animal.getX());

        animal.move(Direction.LEFT);
        assertEquals(5, animal.getY());
        assertEquals(5, animal.getX());
    }


    @Test
    public void testEat() {
        Animal predator = new Animal(1, "Predator", 10, 5, 5, 50, 100, map, time);
        map[5][5].addAnimal(predator);

        Animal prey = new Animal(1, "Prey", 5, 5, 6, 0, 50, map, time);
        map[5][6].addAnimal(prey);

        predator.eat(prey);
        assertEquals(100, predator.getHealth());
        assertFalse(map[5][6].containsAnimal());
    }}
