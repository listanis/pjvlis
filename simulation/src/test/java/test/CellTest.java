package test;
import Animals.Animal;
import Map.Cell;
import Map.Terrain;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CellTest {

    private Cell cell;
    private Animal animal;

    @BeforeEach
    void setUp() {
        cell = new Cell(false, Terrain.GRASS, 0, 0);
        animal = new Animal(1, "TestAnimal", 10, 0, 0, 100, 100, null, null);
    }

    @Test
    void testContainsAnimal() {
        assertFalse(cell.containsAnimal());

        cell.addAnimal(animal);
        assertTrue(cell.containsAnimal());

        cell.removeAnimal();
        assertFalse(cell.containsAnimal());
    }

    @Test
    void testAddAnimal() {
        assertNull(cell.getAnimalOnCell());

        cell.addAnimal(animal);
        assertEquals(animal, cell.getAnimalOnCell());
    }

    @Test
    void testRemoveAnimal() {
        cell.addAnimal(animal);
        assertNotNull(cell.getAnimalOnCell());

        cell.removeAnimal();
        assertNull(cell.getAnimalOnCell());
    }
}
