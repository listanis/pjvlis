package test;

import Controllers.MapCreator;
import Map.Time;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.HashMap;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MapCreatorTest {

    private MapCreator mapCreator;

    @BeforeEach
    void setUp() {
        Time time = new Time("real_time");
        mapCreator = new MapCreator(10, 10, 2, 2, 1, 3, 1, time);
    }

    @Test
    void testCreateMap() {
        mapCreator.createMap();
        assertEquals(10, mapCreator.map.length);
        assertEquals(10, mapCreator.map[0].length);
    }

    @Test
    void testFillMap() {
        mapCreator.createMap();
        HashMap<String, Integer> animalList = new HashMap<>();
        animalList.put("Lions", 2);
        animalList.put("Cheetahs", 2);
        animalList.put("Rhinoceros", 1);
        animalList.put("Zebras", 3);
        animalList.put("Elephants", 1);

        mapCreator.fillMap(animalList);

        assertEquals(2, mapCreator.listOfAmountOfAnimals.get("Lions"));
        assertEquals(2, mapCreator.listOfAmountOfAnimals.get("Cheetahs"));
        assertEquals(1, mapCreator.listOfAmountOfAnimals.get("Rhinoceros"));
        assertEquals(3, mapCreator.listOfAmountOfAnimals.get("Zebras"));
        assertEquals(1, mapCreator.listOfAmountOfAnimals.get("Elephants"));

        int totalAnimals = 0;
        for (int i = 0; i < mapCreator.map.length; i++) {
            for (int j = 0; j < mapCreator.map[0].length; j++) {
                if (mapCreator.map[i][j].containsAnimal()) {
                    totalAnimals++;
                }
            }
        }
        int expectedTotalAnimals = 2 + 2 + 1 + 3 + 1;
        assertEquals(expectedTotalAnimals, totalAnimals);

        int lionsCount = 0, cheetahsCount = 0, rhinocerosCount = 0, zebrasCount = 0, elephantsCount = 0;
        for (int i = 0; i < mapCreator.map.length; i++) {
            for (int j = 0; j < mapCreator.map[0].length; j++) {
                if (mapCreator.map[i][j].containsAnimal()) {
                    String animalType = mapCreator.map[i][j].getAnimalOnCell().getClass().getSimpleName();
                    switch (animalType) {
                        case "Lion" -> lionsCount++;
                        case "Cheetah" -> cheetahsCount++;
                        case "Rhinoceros" -> rhinocerosCount++;
                        case "Zebra" -> zebrasCount++;
                        case "Elephant" -> elephantsCount++;
                    }
                }
            }
        }
        assertEquals(2, lionsCount);
        assertEquals(2, cheetahsCount);
        assertEquals(1, rhinocerosCount);
        assertEquals(3, zebrasCount);
        assertEquals(1, elephantsCount);
    }
}
